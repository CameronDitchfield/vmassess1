#!/bin/bash

apt -y update
apt -y install nginx

cp -f /vagrant/bin/webPageInit.sh /etc/init.d/
chmod 755 /etc/init.d/webPageInit.sh

/etc/init.d/webPageInit.sh start
update-rc.d webPageInit start

# cp /vagrant/bin/index.html /var/www/html/