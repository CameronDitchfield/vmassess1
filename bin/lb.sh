#!/bin/bash

apt -y update
apt -y install haproxy nano

cp -f /vagrant/bin/haproxy.cfg /etc/haproxy/

systemctl enable haproxy
systemctl start haproxy