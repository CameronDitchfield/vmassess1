#!/bin/bash
# description: Set my web page
# chkconfig: 2345 99 99 

case $1 in
    'start')

        touch /var/www/index.html

        # recover info
        . /etc/os-release
        OS=$NAME
        VER=$VERSION_ID

        CPU=$(cat /proc/cpuinfo  | grep 'name'| uniq)

        HOST=$(hostname)

        WEBSERVER=$(nginx -v)

        echo -e "<html>\n<body>\n<h1>\nHello there<br>My IP address is $(ifconfig | grep 192 | awk '{print $2}'| sed 's/^.*://')</h1>\n" >/var/www/html/index.html
        echo -e "<h1>$(hostname)</h1>\n" >>/var/www/html/index.html
        echo -e "<h1>OS: $OS $VER</h1>\n" >>/var/www/html/index.html
        echo -e "<h1>Web Server: $WEBSERVER</h1>\n" >>/var/www/html/index.html
        echo -e "<h1>Num CPUs: $CPU</h1>\n</body>\n</html>" >>/var/www/html/index.html
        ;;
    *)
        echo "Not a valid argument"
        ;;
esac